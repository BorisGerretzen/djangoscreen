# syntax=docker/dockerfile:1
FROM python:3.7.5-slim-buster
ENV PYTHONUNBUFFERED=1
WORKDIR /code
COPY requirements.txt /code/
EXPOSE 8000
RUN apt-get update && apt-get -y install libpq-dev gcc 
RUN pip install -r requirements.txt
COPY . /code/
