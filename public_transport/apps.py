from django.apps import AppConfig
import sys
import os

class PublicTransportConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'public_transport'

    def ready(self):
        if 'runserver' in sys.argv and os.environ.get('RUN_MAIN', None) != 'true' and True:
            import public_transport.updater as updater
            updater.start()
            # updater.run_once()