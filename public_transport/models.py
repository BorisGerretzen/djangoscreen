from django.db import models


class BusTime(models.Model):
    line = models.IntegerField()
    arrival_time = models.DateTimeField()
    destination = models.CharField(max_length=50)
    journey_number = models.IntegerField(default=0, unique=True)