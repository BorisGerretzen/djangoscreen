from apscheduler.schedulers.background import BackgroundScheduler
import public_transport.ovApi


def start():
    scheduler = BackgroundScheduler()
    scheduler.add_job(public_transport.ovApi.update_transport_db, trigger='cron', minute='*/5')
    scheduler.start()

def run_once():
    public_transport.ovApi.update_transport_db()
