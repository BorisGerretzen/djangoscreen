from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
import datetime
from .models import BusTime


def index(request):
    latest_busses = BusTime.objects.order_by('arrival_time')
    template = loader.get_template('public_transport/index.html')
    return HttpResponse(template.render({'busses': latest_busses}, request))

