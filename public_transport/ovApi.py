import requests
import json
from .models import BusTime
import datetime
from django.utils.timezone import make_aware
import pytz


def update_transport_db():
    print("Updating public transport data.")
    tpc = 43003440
    request = requests.get(f"http://v0.ovapi.nl/tpc/{tpc}", headers={
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)'})
    busses = list(json.loads(request.text)[str(tpc)]['Passes'].values())
    BusTime.objects.filter(arrival_time__lt=make_aware(datetime.datetime.now())).delete()
    for bus in busses:
        arrival_time = pytz.timezone('Europe/Amsterdam').localize(
            datetime.datetime.strptime(bus['ExpectedDepartureTime'], '%Y-%m-%dT%H:%M:%S'))
        filtered = BusTime.objects.filter(journey_number=bus['JourneyNumber'])
        if filtered.exists():
            filtered.update(arrival_time=arrival_time)
            continue

        bustime = BusTime()
        bustime.destination = bus['DestinationName50']
        bustime.arrival_time = arrival_time
        bustime.line = bus['LinePublicNumber']
        bustime.journey_number = bus['JourneyNumber']
        bustime.save()
