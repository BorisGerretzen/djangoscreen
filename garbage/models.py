from django.db import models
from django.utils import timezone
import twentemilieu


class GarbagePickup(models.Model):
    class GarbageType(models.IntegerChoices):
        GREY = 1
        GREEN = 2
        PAPER = 3
        PLASTIC = 4

    garbage_type = models.IntegerField(choices=GarbageType.choices)
    pickup_date = models.DateField(unique=True)
    completed = models.BooleanField(default=False)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['garbage_type', 'pickup_date'], name='one garbage a day')
        ]

    def get_garbage_type(self, garbagetype):
        if garbagetype == twentemilieu.const.WASTE_TYPE_NON_RECYCLABLE:
            return self.GarbageType.GREY
        if garbagetype == twentemilieu.const.WASTE_TYPE_ORGANIC:
            return self.GarbageType.GREEN
        if garbagetype == twentemilieu.const.WASTE_TYPE_PAPER:
            return self.GarbageType.PAPER
        if garbagetype == twentemilieu.const.WASTE_TYPE_PLASTIC:
            return self.GarbageType.PLASTIC

    def garbage_type_str(self):
        if self.garbage_type == self.GarbageType.GREY:
            return "Grey"
        if self.garbage_type == self.GarbageType.GREEN:
            return "Green"
        if self.garbage_type == self.GarbageType.PAPER:
            return "Paper"
        if self.garbage_type == self.GarbageType.PLASTIC:
            return "Plastic"

    def days_until(self):
        return (self.pickup_date - timezone.now()).days

    def __str__(self):
        return str(self.pickup_date) + ' - ' + str(self.garbage_type)
