from garbage.models import GarbagePickup
import twentemilieu
import asyncio
from asgiref.sync import sync_to_async


def do_update():
    loop = asyncio.new_event_loop()
    sync_to_async(loop.run_until_complete(__update_garbage_db(loop)))
    loop.close()


@sync_to_async
def __is_duplicate(garbage_type, pickup_date):
    return GarbagePickup.objects.filter(garbage_type=garbage_type,
                                        pickup_date=pickup_date).exists()


async def __update_garbage_db(loop):
    print('Updating garbage days')
    async with twentemilieu.TwenteMilieu(post_code="7542BS", house_number='35', loop=loop) as tw:
        unique_id = await tw.unique_id()
        print("Unique Address ID:", unique_id)
        await tw.update()

        for garbage_type in twentemilieu.const.API_TO_WASTE_TYPE.values():
            pickup = await tw.next_pickup(garbage_type)

            garbage_pickup = GarbagePickup()

            garbage_pickup.garbage_type = garbage_pickup.get_garbage_type(garbage_type)
            garbage_pickup.pickup_date = pickup.date()
            garbage_pickup.completed = False
            print(f'Next pickup for {garbage_type}:', pickup)

            try:
                async_save = sync_to_async(garbage_pickup.save)
                await async_save()
            except:
                print("Tried to insert duplicate bus")
