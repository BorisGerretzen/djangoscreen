from apscheduler.schedulers.background import BackgroundScheduler
import garbage.twentemilieuApi as twentemilieuApi
import asyncio

def start():
    scheduler = BackgroundScheduler()
    scheduler.add_job(twentemilieuApi.do_update, trigger='cron', hour='0')
    scheduler.start()

def run_once():
    twentemilieuApi.do_update()
