from django.apps import AppConfig
import sys
import os

class GarbageConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'garbage'

    def ready(self):
        if 'runserver' in sys.argv and os.environ.get('RUN_MAIN', None) != 'true' and True:
            import garbage.updater as updater
            updater.start()
            updater.run_once()
