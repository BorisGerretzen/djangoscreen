from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
import datetime
from .models import GarbagePickup

def index(request):
    latest_pickups = GarbagePickup.objects.order_by('-pickup_date')[0:4][::-1]
    for pickup in latest_pickups:
        pickup.gtype = pickup.garbage_type_str()

        days_difference = (pickup.pickup_date - datetime.date.today()).days
        if days_difference == 0:
            pickup.today = "Pickup today"
        elif days_difference == 1:
            pickup.today = "Today"
        elif days_difference == 2:
            pickup.today = "Tomorrow"
        else:
            pickup.today = "in {} days".format(days_difference)
    # output = ', '.join(map(lambda x: str(x.pickup_date), latest_pickups))
    # return HttpResponse(output)
    template = loader.get_template('garbage/index.html')
    return HttpResponse(template.render({'latest_pickups': latest_pickups}, request))