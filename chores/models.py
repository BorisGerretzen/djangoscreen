from django.db import models


class User(models.Model):
    name = models.CharField(max_length=30)
    garbage_coins = models.IntegerField(default=0)

    def __str__(self):
        return self.name


class Chore(models.Model):
    name = models.CharField(max_length=50)
    frequency = models.IntegerField()

    def __str__(self):
        return self.name


class ChoreEntry(models.Model):
    chore = models.ForeignKey(Chore, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    deadline = models.DateField()
