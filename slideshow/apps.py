from django.apps import AppConfig
import sys


class SlideshowConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'slideshow'

    # def ready(self):
    #     if 'runserver' in sys.argv:
    #         import public_transport.updater as updater
    #         updater.start()
    #         updater.run_once()