from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
import datetime

def index(request):
    template = loader.get_template('slideshow/index.html')
    return HttpResponse(template.render({'pages': ['garbage', 'public_transport'], 'page_time': 15}, request=request))

